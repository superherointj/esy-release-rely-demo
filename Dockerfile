# FROM superherointj/archlinux-esy
# Bellow image is just cache for esy, if you want to build WITHOUT cache use image above. Esy is too slow building first time. 
FROM superherointj/batci:libc-arch-bootstrap

COPY Makefile *.json *.opam *.ml *.mli *.re *.rei dune dune-project ./

RUN esy

RUN esy build

# Will fail unless (1) `demo1tests.install` is added to 'esy.install' field  (2) 'esy.build' field includes package `demo1tests`. Both are undesirable for release.
RUN esy x RunTests 

RUN esy x --release demo1

# Will fail unless I include rely in devDependencies and change 'install' field.
RUN esy release 

# Shouldn't error, but it errors as: `Fatal error: exception Unix.Unix_error(Unix.ENOENT, "execve", "/home/intj/git/sandbox/esy-release-rely-demo/_release/3/i/esy_release_rely_demo1-87c4e520/bin/demo1")`
RUN ./_release/bin/demo1 
