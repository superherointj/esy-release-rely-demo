# esy-release-rely-demo

My failed attempt at getting Esy release w/ Rely right.

I expect the following commands to succeed:
* During development:
```
    esy x RunTests
    esy x demo1
``` 
* During release:
```
    # No Test
    esy x --release demo1
```

My näive expectations for `package.json` were:
```
    "build": "dune build -p demo1",
    "devBuild": "refmterr dune build -p demo1,demo1tests",
    "install": "esy-installer demo1.install",
    "devInstall": "esy-installer demo1.install demo1tests.install",
```
But `devInstall` is not valid. And `esy.release.install` didn't seem to work for me. I'm after an equivalent solution.
That allows me to `esy x RunTests` in development mode and for release `esy release` and `esy x demo1` should also work.

## Questions

## Should a 'devInstall' field exist for allowing different install values for development & release modes so 'esy x' can work for both?

Ideally `RunTests` should run in development mode only, right? If yes, how can I make so?

## Should @reason-native/rely go in dependencies instead of devDependencies?

1) [Esy docs](https://esy.sh/docs/en/next/concepts.html) states: "Dependencies that are required only during development ...are: `@reason-native/rely` as it is only required during development of the root package."
But then Esy's own source code adds `{"@reason-native/rely": "3.1.0"}` to `dependencies` instead of `devDependencies`, why is that?


2) If package `demo1tests` is included in `esy.build`, `npm release` will fail missing `@reason-native/rely` in dependencies, I think I shouldn't be building `demo1tests` package for release but I'm still trying to figure out how to not build it and have `esy x RunTests` to still work during development and `esy x demo1` to work as well in release.

Having package `demo1tests` in `esy.build`, will cause to miss the `@reason-native/rely` dependency:
```
    File "dune", line 19, characters 25-33:
    19 |     (libraries demo1.lib rely.lib)
                                  ^^^^^^^^
    Error: Library "rely.lib" not found.
    Hint: try: dune external-lib-deps --missing -p demo1,demo1tests @instal
 ```
So, it seems I need to avoid adding `demo1tests` in release.

## Building it
```
esy
esy build
esy x RunTests # Will fail unless (1) `demo1tests.install` is added to `esy.install` field  (2) `esy.build` field includes package `demo1tests`. Both are undesirable for release.
esy x --release demo1
esy release # Will fail unless I include rely in devDependencies and change `install` field.
./_release/bin/demo1 # Shouldn't error, but it errors as: `Fatal error: exception Unix.Unix_error(Unix.ENOENT, "execve", "/home/intj/git/sandbox/esy-release-rely-demo/_release/3/i/esy_release_rely_demo1-87c4e520/bin/demo1")`
```

### Docker
For reproducibility, to build docker image, just run `"make"`.
